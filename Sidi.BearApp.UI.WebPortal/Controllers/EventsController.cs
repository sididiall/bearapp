﻿using Sidi.CircusApp.Services.EventManagement;
using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sidi.CircusApp.UI.WebPortal.Controllers
{
    public class EventsController : Controller
    {
        // GET: Events
        public ActionResult Index()
        {
            IEnumerable<Event> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IEventBusiness>();
                result = service.GetEvents();

            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Event model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<IEventBusiness>();
                service.CreateEvents(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            Event result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IEventBusiness>();
                result = service.GetEventById(id);
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult Update(Event model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<IEventBusiness>();
                service.UpdateEvent(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public JsonResult Delete(int id)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<IEventBusiness>();
                service.DeleteEvent(id);
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true
                    }
                );
        }
    }
}