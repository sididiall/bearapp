﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Sidi.CircusApp.Tools.DependencyInjector;
using Sidi.CircusApp.Services.TrainerManagement;
using Sidi.CircusApp.Services.AnimalManagement;
using Sidi.CircusApp.Services.CircusManagement;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Tools.Security;
using Sidi.CircusApp.Data.EntityContext;
using System.Collections;
using Sidi.CircusApp.Services.EventManagement;


namespace UnitTestProject1
{
    [TestClass]
    public class EventServiceUnitTest
    {
        public static IKernel Kernel = new StandardKernel();
        UserContext userCtx = new UserContext();

        [TestMethod]
        public void CreateEventSuccessTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var eventService = Kernel.Get<EventBusiness>();

            userCtx.Set(new ConnectedUser()
            {
                IsAdmin = false,
                TrainerId = 5,
                CircusId = 2
            });

            eventService.CreateEvents(new Event
            {
                AnimalId = 2,
                TrainerId = 5,
                Category = Category.Repas,
                Comments = "grnhkh"
            });

            bool IsCreated = dbCtx.GetContext().Events.Any(e =>
                e.AnimalId == 2 &&
                e.TrainerId == 5
                );

            Assert.IsTrue(IsCreated);
        }

        [TestMethod]
        public void CreateEventExceptTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var eventService = Kernel.Get<EventBusiness>();

            userCtx.Set(new ConnectedUser()
            {
                IsAdmin = false,
                TrainerId = 2,
                CircusId = 1
            });

            try
            {
                eventService.CreateEvents(new Event
                {
                    AnimalId = 2,
                    TrainerId = 4,
                    Category = Category.Nettoyage,
                    Comments = "grh"
                });
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Vous n'êtes pas autorisé à créer des tickets.", e.Message);
            }


        }
    }
}
