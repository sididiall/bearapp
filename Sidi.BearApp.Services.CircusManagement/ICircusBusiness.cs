﻿using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.CircusManagement
{
    public interface ICircusBusiness
    {
        Circus CreateCircus(Circus model);

        IEnumerable<Circus> GetCircuses();

        Circus GetCircusById(int id);

        int UpdateCircus(Circus model);

        void DeleteCircus(int id);

    }
}
