﻿$(function(){
    $.ajax({
        url: '/Users/GetAvailableUsers/',
        type: 'get'
    })
    .done(function (r) {
        if (!r.success) {
            alert("Impossible de récupérer les utilsateurs disponibles.");
        } else {
            $.each(r.data, function (k, v) {
                $(".dev-tool-auth").append(
                        $("<li>")
                            .css("cursor", "pointer")
                            .css("padding", "5px 20px")
                            .text(v.Name)
                            .data("user-id", v.User)
                            .data("user-passwd", v.Password)
                            .click(function () {
                                connectMe(
                                     $(this).data("user-id"),
                                     $(this).data("user-passwd")
                                    )
                            })
                    );
            });
        }
    });

    var connectMe = function (user, passwd) {
        
        $.ajax({
            url: '/Users/ConnectMe/',
            type: 'post',
            data : {
                identifier : user,
                passwd : passwd
            }
        })
        .done(function (r) {            
            if (!r.success) {
                alert("Impossible de changer d'utilisateur");
            } else {
                window.location.reload()
            }
        });
    };

});
