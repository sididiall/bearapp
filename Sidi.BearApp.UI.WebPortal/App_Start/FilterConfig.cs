﻿using System.Web;
using System.Web.Mvc;

namespace Sidi.CircusApp.UI.WebPortal
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
