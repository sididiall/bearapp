﻿using Nest;
using Elasticsearch.Net;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Data.EntityContext;
using Sidi.CircusApp.Tools.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.AnimalManagement
{
    public class AnimalBusiness : IAnimalBusiness
    {
        private readonly IDatabaseFactory _dbFactory;
        private readonly UserContext _userCtx;

        public AnimalBusiness(IDatabaseFactory dbFactory, UserContext userCtx)
        {
            this._dbFactory = dbFactory;
            this._userCtx = userCtx;
        }

        public Animal CreateAnimal(Animal model)
        {
            if (String.IsNullOrWhiteSpace(model.Name))
                throw new FormatException("Attention ! Le nom d'un animal ne peut être vide ou null");

            if (String.IsNullOrWhiteSpace(model.AnimalBreed))
                throw new FormatException("Attention ! La race d'un animal ne peut être vide ou null");

            if (model.Birthdate.Date > DateTime.Today)
                throw new Exception("Attention ! La date choisi ne peut pas depasser la date actuelle");

            if (model.Height.Value < 0 || model.Weight.Value < 0)
                throw new Exception("Attention ! Le nombre saisi ne peut pas être inferieur à zéros");

            if (this._userCtx.Get().IsAdmin == false)
                throw new Exception("Vous n'êtes pas autorisé à créer des animal.");

            using (var context = this._dbFactory.GetContext())
            {
                if (model.FatherId.HasValue)
                {
                    var knownId = context.Animals.Any(e => e.AnimalId == model.FatherId.Value);
                    if (!knownId)
                        throw new Exception("Attention ! Le fatherId d'un animal doit être en base ou null");
                }

                var exists = context.Circuses.Any(e => e.CircusId == model.CircusId);
                if (!exists)
                    throw new Exception("Attention ! le circusId est pas en base de donnée");

                if (model.CircusId == 0)
                    throw new Exception("Attention ! Le circusId d'un dresseur doit être supèrieur à 0 en base");

                if (model.CircusId != this._userCtx.Get().CircusId)
                    throw new Exception("Attention ! Le circusId d'un dresseur doit être le même");

                Animal created = context.Animals.Add(new Animal()
                {
                    Name = model.Name,
                    AnimalBreed = model.AnimalBreed,
                    Birthdate = model.Birthdate,
                    Gender = model.Gender,
                    Height = model.Height,
                    Weight = model.Weight,
                    FatherId = model.FatherId,
                    CircusId = model.CircusId
                });
                context.SaveChanges();

                var result = created;

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(created, idx => idx
                    .Index("circusesmanage-animalindex")
                    .Type("Animal")
                    .Id(created.AnimalId)
                    );

                return result;
            }

        }

        public Animal GetAnimalById(int id)
        {
            var usrctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {

                Animal b = context.Animals
                    .FirstOrDefault(e => e.AnimalId == id && e.CircusId == usrctx.CircusId);
                if (b == null)
                    throw new Exception("Attention ! Le AnimalId d'un animal doit être en base");
                return b;
            }
        }

        public IEnumerable<Animal> GetAnimals()
        {
            using (var context = this._dbFactory.GetContext())
            {
                var usCtxCId = this._userCtx.Get().CircusId;
                IEnumerable<Animal> Animals = context.Animals
                    .Include("Trainers")
                    .Include("Trainers.Circus")
                    .Where(e => e.CircusId == usCtxCId)
                    .ToList();

                return Animals;
            }



        }

        public int UpdateAnimal(Animal model)
        {

            if (String.IsNullOrWhiteSpace(model.Name))
                throw new Exception("Attention ! Le nom d'un animal ne peut être vide ou null");

            if (model.Birthdate.Date > DateTime.Today)
                throw new Exception("Attention ! La date choisi ne peut pas depasser la date actuelle");

            if (model.Height.Value < 0 || model.Weight.Value < 0)
                throw new Exception("Attention ! Le nombre saisi ne peut pas être inferieur à zéros");

            if (model.AnimalId == model.FatherId)
                throw new Exception("Attention ! Un animal ne peut être son propres père !");


            using (var context = this._dbFactory.GetContext())
            {
                if (model.FatherId.HasValue)
                {
                    var knownId = context.Animals.Any(e => e.AnimalId == model.FatherId.Value);
                    if (!knownId)
                        throw new Exception("Attention ! Le fatherId d'un animal doit être en base ou null");
                }

                Animal a = context.Animals.Include("Trainers").FirstOrDefault(e => e.AnimalId == model.AnimalId);

                if (a == null)
                    throw new Exception("Erreur il n'ya pas d'animal specifiés");

                if (!a.Trainers.Any(e => e.TrainerId == _userCtx.Get().TrainerId))
                    throw new Exception("Vous n'etes pas autoriser à modifier cet animal");

                a.Name = model.Name;
                a.AnimalBreed = model.AnimalBreed;
                a.Birthdate = model.Birthdate;
                a.FatherId = model.FatherId;
                a.Gender = model.Gender;
                a.Height = model.Height;
                a.Weight = model.Weight;

                context.SaveChanges();

                var res = a.AnimalId;

                var json = new 
                {
                    a.Name,
                    a.AnimalBreed,
                    a.Birthdate,
                    a.FatherId,
                    a.Gender,
                    a.Height,
                    a.Weight,
                };

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(json, u => u
                    .Index("circusesmanage-animalindex")
                    .Type("Animal")
                    .Id(res));

                return res;
            }

        }

        public void DeleteAnimal(int id)
        {
            var usrctx = this._userCtx.Get();
            if (!this._userCtx.Get().IsAdmin)
                throw new Exception("Vous n'êtes pas autorisé à suprimer des comptes.");

            using (var context = this._dbFactory.GetContext())
            {
                Animal b = context.Animals.FirstOrDefault(e => e.AnimalId == id && e.CircusId == usrctx.CircusId);

                if (b == null)
                    throw new Exception("Attention ! Le AnimalId d'un animal doit être en base");

                var exists = context.Animals.Any(e => e.AnimalId == e.FatherId);
                if (exists)
                    throw new Exception("Attention ! le AnimalID est associé au FatherID d'un animal");
                context.Animals.Remove(b);

                context.SaveChanges();

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Delete<Animal>(id, d => d
                    .Index("circusesmanage-animalindex")
                    .Type("Animal"));
            }

        }

    }
}
