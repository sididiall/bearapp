﻿using Sidi.CircusApp.Tools.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidi.CircusApp.UI.WebPortal.Classes
{
    public static class UserRoles
    {
        private static UserContext _userCtx;


        public static bool isAdmin()
        {
            if (_userCtx == null)
            {
                _userCtx = new UserContext();
            }

            return _userCtx.Get().IsAdmin;
        }

    }
}