
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/19/2016 11:00:17
-- Generated from EDMX file: C:\Users\Sidi\documents\visual studio 2013\Projects\Sidi.BearApp\Sidi.BearApp.Data.EntityContext\EntityContext.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [EF_Sidi];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AnimalAnimal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Animals] DROP CONSTRAINT [FK_AnimalAnimal];
GO
IF OBJECT_ID(N'[dbo].[FK_CircusTrainer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trainers] DROP CONSTRAINT [FK_CircusTrainer];
GO
IF OBJECT_ID(N'[dbo].[FK_AnimalCircus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Animals] DROP CONSTRAINT [FK_AnimalCircus];
GO
IF OBJECT_ID(N'[dbo].[FK_EventAnimal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_EventAnimal];
GO
IF OBJECT_ID(N'[dbo].[FK_EventTrainer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_EventTrainer];
GO
IF OBJECT_ID(N'[dbo].[FK_AnimalTrainer_Animal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnimalTrainer] DROP CONSTRAINT [FK_AnimalTrainer_Animal];
GO
IF OBJECT_ID(N'[dbo].[FK_AnimalTrainer_Trainer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnimalTrainer] DROP CONSTRAINT [FK_AnimalTrainer_Trainer];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Animals]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Animals];
GO
IF OBJECT_ID(N'[dbo].[Trainers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trainers];
GO
IF OBJECT_ID(N'[dbo].[Circuses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Circuses];
GO
IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[AnimalTrainer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AnimalTrainer];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Animals'
CREATE TABLE [dbo].[Animals] (
    [AnimalId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [Birthdate] datetimeoffset  NOT NULL,
    [Height] float  NULL,
    [Weight] int  NULL,
    [FatherId] int  NULL,
    [Gender] tinyint  NOT NULL,
    [CircusId] int  NOT NULL,
    [AnimalBreed] nvarchar(255)  NOT NULL
);
GO

-- Creating table 'Trainers'
CREATE TABLE [dbo].[Trainers] (
    [TrainerId] int IDENTITY(1,1) NOT NULL,
    [User] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Admin] bit  NOT NULL,
    [CircusId] int  NOT NULL
);
GO

-- Creating table 'Circuses'
CREATE TABLE [dbo].[Circuses] (
    [CircusId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [Country] nvarchar(255)  NOT NULL,
    [City] nvarchar(255)  NOT NULL,
    [Description] nvarchar(255)  NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [EventId] int IDENTITY(1,1) NOT NULL,
    [AnimalId] int  NOT NULL,
    [TrainerId] int  NOT NULL,
    [Comments] nvarchar(max)  NOT NULL,
    [Category] tinyint  NOT NULL
);
GO

-- Creating table 'AnimalTrainer'
CREATE TABLE [dbo].[AnimalTrainer] (
    [Animals_AnimalId] int  NOT NULL,
    [Trainers_TrainerId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AnimalId] in table 'Animals'
ALTER TABLE [dbo].[Animals]
ADD CONSTRAINT [PK_Animals]
    PRIMARY KEY CLUSTERED ([AnimalId] ASC);
GO

-- Creating primary key on [TrainerId] in table 'Trainers'
ALTER TABLE [dbo].[Trainers]
ADD CONSTRAINT [PK_Trainers]
    PRIMARY KEY CLUSTERED ([TrainerId] ASC);
GO

-- Creating primary key on [CircusId] in table 'Circuses'
ALTER TABLE [dbo].[Circuses]
ADD CONSTRAINT [PK_Circuses]
    PRIMARY KEY CLUSTERED ([CircusId] ASC);
GO

-- Creating primary key on [EventId] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([EventId] ASC);
GO

-- Creating primary key on [Animals_AnimalId], [Trainers_TrainerId] in table 'AnimalTrainer'
ALTER TABLE [dbo].[AnimalTrainer]
ADD CONSTRAINT [PK_AnimalTrainer]
    PRIMARY KEY CLUSTERED ([Animals_AnimalId], [Trainers_TrainerId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [FatherId] in table 'Animals'
ALTER TABLE [dbo].[Animals]
ADD CONSTRAINT [FK_AnimalAnimal]
    FOREIGN KEY ([FatherId])
    REFERENCES [dbo].[Animals]
        ([AnimalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AnimalAnimal'
CREATE INDEX [IX_FK_AnimalAnimal]
ON [dbo].[Animals]
    ([FatherId]);
GO

-- Creating foreign key on [CircusId] in table 'Trainers'
ALTER TABLE [dbo].[Trainers]
ADD CONSTRAINT [FK_CircusTrainer]
    FOREIGN KEY ([CircusId])
    REFERENCES [dbo].[Circuses]
        ([CircusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CircusTrainer'
CREATE INDEX [IX_FK_CircusTrainer]
ON [dbo].[Trainers]
    ([CircusId]);
GO

-- Creating foreign key on [CircusId] in table 'Animals'
ALTER TABLE [dbo].[Animals]
ADD CONSTRAINT [FK_AnimalCircus]
    FOREIGN KEY ([CircusId])
    REFERENCES [dbo].[Circuses]
        ([CircusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AnimalCircus'
CREATE INDEX [IX_FK_AnimalCircus]
ON [dbo].[Animals]
    ([CircusId]);
GO

-- Creating foreign key on [AnimalId] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [FK_EventAnimal]
    FOREIGN KEY ([AnimalId])
    REFERENCES [dbo].[Animals]
        ([AnimalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventAnimal'
CREATE INDEX [IX_FK_EventAnimal]
ON [dbo].[Events]
    ([AnimalId]);
GO

-- Creating foreign key on [TrainerId] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [FK_EventTrainer]
    FOREIGN KEY ([TrainerId])
    REFERENCES [dbo].[Trainers]
        ([TrainerId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventTrainer'
CREATE INDEX [IX_FK_EventTrainer]
ON [dbo].[Events]
    ([TrainerId]);
GO

-- Creating foreign key on [Animals_AnimalId] in table 'AnimalTrainer'
ALTER TABLE [dbo].[AnimalTrainer]
ADD CONSTRAINT [FK_AnimalTrainer_Animal]
    FOREIGN KEY ([Animals_AnimalId])
    REFERENCES [dbo].[Animals]
        ([AnimalId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Trainers_TrainerId] in table 'AnimalTrainer'
ALTER TABLE [dbo].[AnimalTrainer]
ADD CONSTRAINT [FK_AnimalTrainer_Trainer]
    FOREIGN KEY ([Trainers_TrainerId])
    REFERENCES [dbo].[Trainers]
        ([TrainerId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AnimalTrainer_Trainer'
CREATE INDEX [IX_FK_AnimalTrainer_Trainer]
ON [dbo].[AnimalTrainer]
    ([Trainers_TrainerId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------