﻿using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Data.EntityContext;
using Sidi.CircusApp.Tools.Security;
using Sidi.CircusApp.Services.TrainerManagement;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace Sidi.CircusApp.Services.UserManagement
{
    public class UserBusiness : IUserBusiness
    {
        private readonly IDatabaseFactory _dbFactory;
        private readonly UserContext _userCtx;

        public UserBusiness(IDatabaseFactory dbFactory, UserContext userCtx)
        {
            this._dbFactory = dbFactory;
            this._userCtx = userCtx;
        }

        public IEnumerable<Trainer> GetUsers()
        {
            var userctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                IEnumerable<Trainer> t = context.Trainers
                    //.Where(e => e.CircusId == userctx.CircusId)
                    .ToList();

                if (t == null)
                    throw new Exception("Erreur ! Il n'ya pas d'user à ce nom dans ce cirque");
                return t;
            }
        }

        public Trainer AuthMethod(string user, string password)
        {
            using (var context = this._dbFactory.GetContext())
            {
                Trainer t = context.Trainers.FirstOrDefault(e =>
                    e.User == user
                    && e.Password == password);

                if (t == null)
                    throw new Exception("Les identifiants ou mdp ne sont pas bons");
                UserContext uc = new UserContext();
                uc.Set(new ConnectedUser()
                {
                    TrainerId = t.TrainerId,
                    CircusId = t.CircusId,
                    IsAdmin = t.Admin
                });
                return t;
            }
        }
    }
}
