﻿$(function () {
    $.ajax({
        url: '/Trainers/GetTrainerId/',
        type: 'get'
    })
    .done(function (r) {
        if (!r.success) {
            alert("Impossible de récupérer l'id.");
        } else {
            $(".get-trainerId").append(
                $("<li>")
                    .css("cursor", "pointer")
                    .css("padding", "5px 20px")
                    .text(r.data.name)
                    .click(function () {
                        var trainerid = r.data.id;
                        var url = "/Trainers/Details/" + trainerid;
                        window.location.href = url
                    })
            );
        }
    })
});