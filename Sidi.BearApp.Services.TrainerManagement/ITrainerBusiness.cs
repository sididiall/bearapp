﻿using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.TrainerManagement
{
    public interface ITrainerBusiness
    {
        /// <summary>
        /// Ajoutes un dresseeur en base et retourne son id
        /// </summary>
        /// <param name="model"></param>
        /// <returns>int</returns>
        Trainer CreateTrainer(Trainer model);

        /// <summary>
        ///  Permet de récupérer le dresseur en base par son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>id</returns>
        Trainer GetTrainerById(int id);

        Trainer GetConnectedTrainer();

        IEnumerable<Trainer> GetTrainers();

        Circus GetCircusData();

        int UpdateTrainer(Trainer model);

        void DeleteTrainer(int id);

        void AffectTrainerToAnimal(int TrainerId, int BearId);

        void DisaffectTrainerToAnimal(int TrainerId, int BearId);
    }
}
