﻿using Nest;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Data.EntityContext;
using Sidi.CircusApp.Tools.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.EventManagement
{
    public class EventBusiness : IEventBusiness
    {
        private readonly IDatabaseFactory _dbFactory;
        private readonly UserContext _userCtx;
        public EventBusiness(IDatabaseFactory dbFactory, UserContext userCtx)
        {
            this._dbFactory = dbFactory;
            this._userCtx = userCtx;
        }

        public Event CreateEvents(Event model)
        {
            var uctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                var existBr = context.Animals.Include("Trainers").Any(e => e.AnimalId == model.AnimalId);

                Animal b = context.Animals.Include("Trainers").FirstOrDefault(e => e.AnimalId == model.AnimalId);

                if (!existBr)
                    throw new Exception("L'Animal saisie n'existes pas en base");

                if (!b.Trainers.Any(e => e.TrainerId == uctx.TrainerId))
                    throw new Exception("Vous n'êtes pas autorisé à créer des tickets.");

                Event create = context.Events.Add(new Event
                {
                    AnimalId = model.AnimalId,
                    TrainerId = model.TrainerId,
                    Category = model.Category,
                    Comments = model.Comments
                });

                context.SaveChanges();

                var result = create;

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(create, idx => idx
                    .Index("circusesmanage-eventindex")
                    .Type("Event")
                    .Id(create.EventId)
                    );

                return result;
            }
        }

        public IEnumerable<Event> GetEvents()
        {
            using (var context = this._dbFactory.GetContext())
            {
                var usctx = this._userCtx.Get();
                IEnumerable<Event> Events = context.Events
                    .Where(e => e.TrainerId == usctx.TrainerId)
                    .ToList();

                return Events;
            }
        }

        public Event GetEventById(int id)
        {
            var usrctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                Event ev = context.Events
                    .FirstOrDefault(e => e.EventId == id && e.TrainerId == usrctx.TrainerId);
                if (ev == null)
                    throw new Exception("Attention ! Le ticket d'un dresseur doit être en base");
                return ev;
            }
        }

        public int UpdateEvent(Event model)
        {
            var uctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                var existBr = context.Animals.Include("Trainers").Any(e => e.AnimalId == model.AnimalId);

                Animal b = context.Animals.Include("Trainers").FirstOrDefault(e => e.AnimalId == model.AnimalId);

                Event ev = context.Events.FirstOrDefault(e => e.EventId == model.EventId);

                if (!existBr)
                    throw new Exception("L'animal saisie n'existes pas en base");

                if (!b.Trainers.Any(e => e.TrainerId == uctx.TrainerId))
                    throw new Exception("Vous n'êtes pas autorisé à créer des tickets.");

                ev.AnimalId = model.AnimalId;
                ev.TrainerId = model.TrainerId;
                ev.Category = model.Category;
                ev.Comments = model.Comments;

                context.SaveChanges();

                var result = ev.EventId;

                var json = new
                {
                    ev.AnimalId,
                    ev.TrainerId,
                    ev.Category,
                    ev.Comments
                };

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(json, u => u
                    .Index("circusesmanage-eventindex")
                    .Type("Event")
                    .Id(result));

                return result;
            }
        }

        public void DeleteEvent(int id)
        {
            var usrctx = this._userCtx.Get();

            using (var context = this._dbFactory.GetContext())
            {
                Event ev = context.Events.FirstOrDefault(e => e.EventId == id && e.TrainerId == usrctx.TrainerId);

                if (ev == null)
                    throw new Exception("Attention ! Le AnimalId d'un animal doit être en base");

                var exists = context.Animals.Any(e => e.AnimalId == e.FatherId);
                if (exists)
                    throw new Exception("Attention ! le AnimalID est associé au FatherID d'un animal");

                context.Events.Remove(ev);

                context.SaveChanges();

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Delete<Event>(id, d => d
                    .Index("circusesmanage-eventindex")
                    .Type("Event"));
            }
        }


    }
}
