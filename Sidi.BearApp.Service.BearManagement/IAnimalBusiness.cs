﻿using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Sidi.CircusApp.Services.AnimalManagement
{
    public interface IAnimalBusiness
    {
        /// <summary>
        ///  Ajoute un ours en base et retourne son identifiant
        /// </summary>
        /// <param name="model"></param>
        /// <returns>int</returns>

        Animal CreateAnimal(Animal model);
        
        /// <summary>
        ///  Permet de récupèrer un ours grâce à son ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bear</returns>
        Animal GetAnimalById(int id);

        /// <summary>
        /// Permet de récupérer tous les ours en base
        /// </summary>
        /// <returns></returns>

        IEnumerable<Animal> GetAnimals();

        /// <summary>
        ///  Permet de mettre à jour ....
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        int UpdateAnimal(Animal model);

        /// <summary>
        ///  Suppr...
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void DeleteAnimal(int id);
    }
}