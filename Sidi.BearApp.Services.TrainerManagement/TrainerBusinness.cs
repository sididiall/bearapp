﻿using Nest;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Data.EntityContext;
using Sidi.CircusApp.Tools.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.TrainerManagement
{
    public class TrainerBusinness : ITrainerBusiness
    {
        private readonly IDatabaseFactory _dbFactory;
        private readonly UserContext _userCtx;

        public TrainerBusinness(IDatabaseFactory dbFactory, UserContext userCtx)
        {
            this._dbFactory = dbFactory;
            this._userCtx = userCtx;
        }

        public Trainer CreateTrainer(Trainer model)
        {
            if (this._userCtx.Get().IsAdmin == false)
                throw new Exception("Vous n'êtes pas autorisé à créer des comptes.");

            if (String.IsNullOrWhiteSpace(model.Name))
                throw new FormatException("Attention ! Le nom d'un dresseur ne peut être vide ou null");

            using (var context = this._dbFactory.GetContext())
            {
                var exists = context.Circuses.Any(e => e.CircusId == model.CircusId);
                if (!exists)
                    throw new Exception("Attention ! le circusId est pas en base de donnée");
                if (model.CircusId == 0)
                    throw new Exception("Attention ! Le circusId d'un dresseur doit être supèrieur à 0 en base");

                Trainer created = context.Trainers.Add(new Trainer()
                {
                    User = model.User,
                    Password = model.Password,
                    Name = model.Name,
                    Admin = model.Admin,
                    CircusId = model.CircusId
                });
                context.SaveChanges();

                var result = created;

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(created, idx => idx
                    .Index("circusesmanage-trainerindex")
                    .Type("Trainer")
                    .Id(created.TrainerId)
                    );

                return result;
            }
        }

        public Trainer GetConnectedTrainer()
        {
            var connectedUs = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                Trainer t = context.Trainers.FirstOrDefault(e => e.TrainerId == connectedUs.TrainerId);
                if (t == null)
                    throw new Exception("Attention ! Le TrainerId d'un dresseur doit être en base");
                return t;
            }
        }

        public Trainer GetTrainerById(int id)
        {
            var utctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                Trainer t = context.Trainers.FirstOrDefault(e => e.TrainerId == id && e.CircusId == utctx.CircusId);
                if (t == null)
                    throw new Exception("Attention ! Le TrainerId d'un dresseur doit être en base");
                return t;
            }
        }

        public IEnumerable<Trainer> GetTrainers()
        {
            using (var context = this._dbFactory.GetContext())
            {
                var usctx = this._userCtx.Get();
                IEnumerable<Trainer> trainers = context.Trainers
                    //
                    .Where(e => e.CircusId == usctx.CircusId)
                    .ToList();

                return trainers;
            }
        }

        public Circus GetCircusData()
        {
            var connectedUser = this._userCtx.Get();

            using (var context = this._dbFactory.GetContext())
            {
                Circus c = context.Circuses.FirstOrDefault(e => e.CircusId == connectedUser.CircusId);
                if (c == null)
                    throw new Exception("Le cirque n'existe pas dans le contexte");
                return c;
            }
        }

        public int UpdateTrainer(Trainer model)
        {
            var uCtx = this._userCtx.Get();
            if (uCtx.IsAdmin == false)
                throw new Exception("Vous n'êtes pas autorisé à modifier des comptes.");

            if (String.IsNullOrWhiteSpace(model.Name))
                throw new FormatException("Attention ! Le nom d'un dresseur ne peut être vide ou null");

            using (var context = this._dbFactory.GetContext())
            {
                Trainer t = context.Trainers.FirstOrDefault(e =>
                        e.TrainerId == model.TrainerId
                        && e.CircusId == uCtx.CircusId
                    );

                if (t == null)
                    throw new Exception("Le trainer n'existe pas");

                t.User = model.User;
                t.Password = model.Password;
                t.Name = model.Name;
                t.Admin = model.Admin;
                t.CircusId = model.CircusId;

                context.SaveChanges();

                var result = t.TrainerId;

                var json = new
                {
                    t.User,
                    t.Password,
                    t.Name,
                    t.Admin,
                    t.CircusId
                };

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(json, u => u
                    .Index("circusesmanage-Trainerindex")
                    .Type("Trainer")
                    .Id(result));

                return result;
            }
        }

        public void DeleteTrainer(int id)
        {
            using (var context = this._dbFactory.GetContext())
            {
                if (!this._userCtx.Get().IsAdmin)
                    throw new Exception("Vous n'êtes pas autorisé à suprimer des comptes.");
                Trainer t = context.Trainers.FirstOrDefault(e => e.TrainerId == id);

                if (t == null)
                    throw new Exception("Attention ! Le dresseur d'un Animal n'existe pas");

                context.Trainers.Remove(t);

                context.SaveChanges();

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Delete<Event>(id, d => d
                    .Index("circusesmanage-Trainerindex")
                    .Type("Trainer"));
            }

        }

        public void AffectTrainerToAnimal(int TrainerId, int AnimalId)
        {
            var uCtx = this._userCtx.Get();

            if (this._userCtx.Get().IsAdmin == false)
                throw new Exception("Vous n'êtes pas autorisé à faire cette action.");

            using (var context = this._dbFactory.GetContext())
            {
                Trainer t = context.Trainers
                    .Include("Animals")
                    .FirstOrDefault(e => e.TrainerId == TrainerId
                    && e.CircusId == uCtx.CircusId);

                Animal a = context.Animals
                    .Include("Trainers")
                    .FirstOrDefault(e => e.AnimalId == AnimalId
                    && e.CircusId == uCtx.CircusId);

                if (t == null || a == null)
                    throw new Exception("Il n'ya pas d'Animal ou de dresseur en base");
                if (t.CircusId != a.CircusId)
                    throw new Exception("Le circusID ne sont pas les mm");

                t.Animals.Add(a);
                a.Trainers.Add(t);

                context.SaveChanges();
            }
        }
        public void DisaffectTrainerToAnimal(int TrainerId, int AnimalId)
        {
            var uCtx = this._userCtx.Get();

            if (!this._userCtx.Get().IsAdmin)
                throw new Exception("Vous n'êtes pas autorisé à faire cette action.");

            using (var context = this._dbFactory.GetContext())
            {
                Trainer t = context.Trainers
                    .Include("Animals")
                    .FirstOrDefault(e => e.TrainerId == TrainerId
                    && e.CircusId == uCtx.CircusId);

                Animal a = context.Animals
                    .Include("Trainers")
                    .FirstOrDefault(e => e.AnimalId == AnimalId
                    && e.CircusId == uCtx.CircusId);

                if (t == null || a == null)
                    throw new Exception("Il n'ya pas d'Animal ou de dresseur en base");
                if (t.CircusId != a.CircusId)
                    throw new Exception("Le circusID ne sont pas les mm");

                t.Animals.Remove(a);
                a.Trainers.Remove(t);
                context.SaveChanges();
            }
        }
    }
}
