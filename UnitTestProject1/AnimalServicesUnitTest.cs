﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Sidi.CircusApp.Tools.DependencyInjector;
using Sidi.CircusApp.Services.TrainerManagement;
using Sidi.CircusApp.Services.AnimalManagement;
using Sidi.CircusApp.Services.CircusManagement;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Tools.Security;
using Sidi.CircusApp.Data.EntityContext;
using System.Collections;


namespace UnitTestProject1
{

    [TestClass]
    public class AnimalServicesUnitTest
    {
        public static IKernel Kernel = new StandardKernel();
        UserContext userCtx = new UserContext();

        [TestMethod]
        public void CreateAnimalExceptionTest()
        {
            Injector.Inject(Kernel);

            var AnimalService = Kernel.Get<AnimalBusiness>();

            // On utilise le connected user par défaut
            // Validation de la création quand le connected user est admin
            try
            {
                AnimalService.CreateAnimal(new Animal()
                {
                    Name = "Gloria",
                    AnimalBreed = "Hippopotame",
                    Birthdate = new DateTime(2009, 09, 10),
                    FatherId = null,
                    Gender = AnimalGender.Female,
                    Height = 130,
                    Weight = 645,
                    CircusId = 1
                });
                // Vérifier la présence de cet ours en base

            }
            catch (Exception e)
            {
                // Aucune exception ne doit survenir
                Assert.Fail(String.Concat("An exception have been thrown : ", e.Message));
            }

        }

        [TestMethod]
        public void UpdateAnimalsExceptionTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var AnimalService = Kernel.Get<AnimalBusiness>();

            //this.userCtx.Set(new ConnectedUser
            //{
            //    CircusId = 1,
            //    IsAdmin = false,
            //    TrainerId = 3
            //});

            AnimalService.UpdateAnimal(new Animal()
            {
                AnimalId = 6,
                Name = "Max",
                AnimalBreed = "Pelican",
                Birthdate = DateTime.UtcNow,
                FatherId = null,
                Gender = AnimalGender.Male,
                Height = 25,
                Weight = 25,
                CircusId = 1
            });

        }

        [TestMethod]
        public void DeleteAnimal()
        {
            Injector.Inject(Kernel);

            var AnimalService = Kernel.Get<AnimalBusiness>();

            AnimalService.DeleteAnimal(7);
        }
    }
}
