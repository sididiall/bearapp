﻿using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Services.AnimalManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sidi.CircusApp.UI.WebPortal.Controllers
{
    public class AnimalsController : Controller
    {

        // GET: Animals
        public ActionResult Index()
        {

            IEnumerable<Animal> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IAnimalBusiness>();

                result = service.GetAnimals();
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }
        
        // Get: Animal by Id
        [HttpGet]
        public ActionResult Details(int id)
        {
            Animal result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IAnimalBusiness>();
                result = service.GetAnimalById(id);
            }
            catch (Exception)
            {
                return RedirectToAction("MyError", "Home");
            }
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }
        // Create: Animals
        [HttpPost]
        public ActionResult Create(Animal model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<IAnimalBusiness>();
                service.CreateAnimal(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            Animal result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IAnimalBusiness>();
                result = service.GetAnimalById(id);
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult Update(Animal model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<IAnimalBusiness>();
                service.UpdateAnimal(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public JsonResult Delete(int id)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<IAnimalBusiness>();
                service.DeleteAnimal(id);
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true
                    }
                );
        }

    }
}