﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yabawt.Tools.ConfigurationManagement;

namespace Sidi.CircusApp.Data.EntityContext
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private readonly IConfigurationManager _conf;
        public DatabaseFactory(IConfigurationManager conf)
        {
            this._conf = conf;
        }

        public EntityContextContainer1 GetContext()
        {
            return new EntityContextContainer1(this._conf);
        }
    } 
}
