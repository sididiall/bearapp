﻿using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Services.TrainerManagement;
using Sidi.CircusApp.Services.CircusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidi.CircusApp.Services.AnimalManagement;

namespace Sidi.CircusApp.UI.WebPortal.Controllers
{
    public class TrainersController : Controller
    {
        // GET: Trainers
        public ActionResult Index()
        {
            IEnumerable<Trainer> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                result = service.GetTrainers();
                    
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        public ActionResult Details(int id)
        {
            Trainer result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                result = service.GetTrainerById(id);
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        public JsonResult GetTrainerId()
        {
            Trainer result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                result = service.GetConnectedTrainer();
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true,
                        data = new
                        {
                            name = result.Name,
                            id = result.TrainerId
                        }
                    },
                    JsonRequestBehavior.AllowGet
                );
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Trainer model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                service.CreateTrainer(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            Trainer result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                result = service.GetTrainerById(id);
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult Update(Trainer model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                service.UpdateTrainer(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Details");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                service.DeleteTrainer(id);
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true
                    }
                );
        }

        public ActionResult GetCircusData()
        {
            Circus result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                result = service.GetCircusData();
            }
            catch (Exception)
            {
                return View();
            }

            return View(result);
        }

        public ActionResult AffectTrainer(int id)
        {
            IEnumerable<Animal> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                var serviceB = DependencyResolver.Current.GetService<IAnimalBusiness>();

                // On essaie de récupérer le trainer
                service.GetTrainerById(id);
                // Récupérer le trainer
                ViewBag.TrainerId = id;  

                // Récupérer les ours
                result = serviceB.GetAnimals();
            }
            catch (Exception)
            {
                return View("MyError","Home");
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult AffectTrainer(int TrainerId, int BearId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                service.AffectTrainerToAnimal(TrainerId, BearId);
            }
            catch (Exception)
            {
                return View();
            }
            return RedirectToAction("Index", "Bears");
        }

        public ActionResult DisaAffectTrainer(int id)
        {
            IEnumerable<Animal> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                var serviceB = DependencyResolver.Current.GetService<IAnimalBusiness>();

                // On essaie de récupérer le trainer
                service.GetTrainerById(id);
                // Récupérer le trainer
                ViewBag.TrainerId = id;

                // Récupérer les ours
                result = serviceB.GetAnimals();
            }
            catch (Exception)
            {
                return View("MyError", "Home");
            }
            return View(result);
        }
        
        [HttpPost]
        public ActionResult DisaAffectTrainer(int TrainerId, int BearId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ITrainerBusiness>();
                service.DisaffectTrainerToAnimal(TrainerId, BearId);
            }
            catch (Exception)
            {
                return View();
            }
            return RedirectToAction("Index", "Bears");
        }
    }
}