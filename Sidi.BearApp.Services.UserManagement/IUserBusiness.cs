﻿using System;
using Sidi.CircusApp.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sidi.CircusApp.Services.TrainerManagement;


namespace Sidi.CircusApp.Services.UserManagement
{
    public interface IUserBusiness
    {
        IEnumerable<Trainer> GetUsers();
        Trainer AuthMethod(string user, string password);
    }
}
