﻿using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.EventManagement
{
    public interface IEventBusiness
    {
        Event CreateEvents(Event model);

        IEnumerable<Event> GetEvents();

        Event GetEventById(int id);

        int UpdateEvent(Event model);

        void DeleteEvent(int id);
    }
}
