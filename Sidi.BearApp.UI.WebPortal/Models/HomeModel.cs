﻿using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidi.CircusApp.UI.WebPortal.Models
{
    public class HomeModel
    {
        public IEnumerable<Animal> listB = new List<Animal>();
        public IEnumerable<Trainer> listT = new List<Trainer>();
        public IEnumerable<Event> listE = new List<Event>();
    }
}