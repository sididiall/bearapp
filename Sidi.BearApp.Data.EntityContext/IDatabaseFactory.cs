﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sidi.CircusApp.Data.EntityContext
{
    public interface IDatabaseFactory
    {
        EntityContextContainer1 GetContext();
    }
}
