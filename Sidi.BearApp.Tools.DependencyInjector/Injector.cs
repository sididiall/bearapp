﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sidi.CircusApp.Data.EntityContext;
using Yabawt.Tools.ConfigurationManagement;
using Sidi.CircusApp.Services.AnimalManagement;
using Sidi.CircusApp.Services.TrainerManagement;
using Sidi.CircusApp.Services.CircusManagement;
using Sidi.CircusApp.Services.EventManagement;
using Sidi.CircusApp.Services.UserManagement;

namespace Sidi.CircusApp.Tools.DependencyInjector
{
    public static class Injector
    {
        public static void Inject(IKernel kernel)
        {

            // Data
            kernel.Bind<IDatabaseFactory>().To<DatabaseFactory>();
            kernel.Bind<IConfigurationManager>().To<AppSettingsConfigurationManager>();

            // Services
            kernel.Bind<IAnimalBusiness>().To<AnimalBusiness>();
            kernel.Bind<ITrainerBusiness>().To<TrainerBusinness>();
            kernel.Bind<ICircusBusiness>().To<CircusBusiness>();
            kernel.Bind<IEventBusiness>().To<EventBusiness>();
            kernel.Bind<IUserBusiness>().To<UserBusiness>();
        } 
    }
}
