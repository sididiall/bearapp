﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Tools.Security
{
    public class UserContext
    {
        static ConnectedUser _cu = new ConnectedUser()
        {
            TrainerId = 1,
            CircusId = 1,
            IsAdmin = true
        };

        public ConnectedUser Get()
        {
            return _cu;
        }
        public void Set(ConnectedUser cu)
        {
            UserContext._cu = cu;
        }

    }

    public class ConnectedUser
    {
        public int TrainerId { get; set; }
        public int CircusId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
