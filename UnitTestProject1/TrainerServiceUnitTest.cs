﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Sidi.CircusApp.Tools.DependencyInjector;
using Sidi.CircusApp.Services.TrainerManagement;
using Sidi.CircusApp.Services.AnimalManagement;
using Sidi.CircusApp.Services.CircusManagement;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Tools.Security;
using Sidi.CircusApp.Data.EntityContext;
using System.Collections;

namespace UnitTestProject1
{

    [TestClass]
    public class TrainerServiceUnitTest
    {
        public static IKernel Kernel = new StandardKernel();
        UserContext userCtx = new UserContext();

        [TestMethod]
        public void CreateTrainerSuccessTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            Guid uniqueIdentifier = Guid.NewGuid();

            String name = "Colin" + uniqueIdentifier.ToString();

            trainerService.CreateTrainer(new Trainer()
            {
                Name = name,
                User = "ColTue",
                Password = "GEdgffth",
                Admin = false,
                CircusId = 2,
            });

            bool isCreated = dbCtx.GetContext().Trainers.Any(t =>
                t.Name == name &&
                t.User == "ColTue" &&
                t.Password == "GEdgffth" &&
                !t.Admin &&
                t.CircusId == 2
            );
            Assert.IsTrue(isCreated);
        }

        [TestMethod]
        public void CreateTrainerInNotAdminTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            //// Ici on est pas Admin
            //userCtx.Set(new ConnectedUser()
            //{
            //    IsAdmin = false,
            //    TrainerId = 2,
            //    CircusId = 1
            //});

            try
            {
                trainerService.CreateTrainer(new Trainer()
                {
                    Name = "Franklin Ceadur",
                    User = "Fnc",
                    Password = "Frankfrank",
                    Admin = true,
                    CircusId = 1,
                });
                Assert.Fail("An exception should have been thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual("Vous n'êtes pas autorisé à créer des comptes.", e.Message);
            }
        }

        [TestMethod]
        public void GetCircusDataTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            userCtx.Set(new ConnectedUser()
            {
                IsAdmin = false,
                TrainerId = 2,
                CircusId = 2
            });

            var ustcId = userCtx.Get().CircusId;
            var usttId = userCtx.Get().TrainerId;

            var tc = trainerService.GetCircusData();

            var t = dbCtx.GetContext().Trainers.FirstOrDefault(e =>
                e.TrainerId == usttId
                );

            Assert.AreEqual(t.CircusId, tc.CircusId, "Vous ne pouvez acceder au cirque qui ne correspond pas");
        }

        [TestMethod]
        public void UpdateTrainerSuccessTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            trainerService.UpdateTrainer(new Trainer()
            {
                TrainerId = 4,
                Name = "hhhhh dougy",
                User = "DougLeBad",
                Password = "GEdgth",
                CircusId = 1,
            });
        }

        [TestMethod]
        public void UpdateTrainerExceptTest()
        {
            Injector.Inject(Kernel);

            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            userCtx.Set(new ConnectedUser()
            {
                IsAdmin = false,
                TrainerId = 2,
                CircusId = 1
            });

            try
            {
                trainerService.UpdateTrainer(new Trainer()
                {
                    TrainerId = 3,
                    Name = "Douggy dougy",
                    User = "DougLeBad",
                    Password = "GEdgth",
                    Admin = false,
                    CircusId = 2,
                });
                Assert.Fail("An exception should have been thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual("Vous n'êtes pas autorisé à modifier des comptes.", e.Message);
            }
        }

        [TestMethod]
        public void AffectTrainerToAnimalTest()
        {
            Injector.Inject(Kernel);

            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            var TrainerId = 1;
            var AnimalId = 5;

            trainerService.AffectTrainerToAnimal(TrainerId, AnimalId);

            bool isvalid = dbCtx.GetContext()
                .Trainers
                .Include("Animals")
                .Any(t =>
                    t.TrainerId == TrainerId && t.Animals.Any(b => b.AnimalId == AnimalId)
                );
            Assert.IsTrue(isvalid);
        }

        [TestMethod]
        public void AffectTrainerToAnimalExceptTest()
        {
            Injector.Inject(Kernel);
            var dbCtx = Kernel.Get<DatabaseFactory>();
            var trainerService = Kernel.Get<TrainerBusinness>();

            userCtx.Set(new ConnectedUser()
            {
                IsAdmin = false,
                TrainerId = 2,
                CircusId = 1
            });

            try
            {
                trainerService.AffectTrainerToAnimal(4, 2);
                Assert.Fail("An exception should have been thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual("Vous n'êtes pas autorisé à faire cette action.", e.Message);
            }
        }
    }
}
