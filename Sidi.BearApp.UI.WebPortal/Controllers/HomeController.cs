﻿using Sidi.CircusApp.Services.AnimalManagement;
using Sidi.CircusApp.Services.EventManagement;
using Sidi.CircusApp.Services.TrainerManagement;
using Sidi.CircusApp.UI.WebPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sidi.CircusApp.UI.WebPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var serviceB = DependencyResolver.Current.GetService<IAnimalBusiness>();
            var serviceT = DependencyResolver.Current.GetService<ITrainerBusiness>();
            var serviceE = DependencyResolver.Current.GetService<IEventBusiness>();

            var connected = serviceT.GetConnectedTrainer();
            HomeModel homeM = new HomeModel()
            {
                listB = serviceB.GetAnimals(),
                listT = serviceT.GetTrainers(),
                listE = serviceE.GetEvents()
            };

            return View(homeM);
        }
        public ActionResult MyError()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}