﻿using Nest;
using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Data.EntityContext;
using Sidi.CircusApp.Tools.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sidi.CircusApp.Services.CircusManagement
{
    public class CircusBusiness : ICircusBusiness
    {
        private readonly IDatabaseFactory _dbFactory;
        private readonly UserContext _userCtx;

        public CircusBusiness(IDatabaseFactory dbFactory, UserContext userCtx)
        {
            this._dbFactory = dbFactory;
            this._userCtx = userCtx;
        }

        public Circus CreateCircus(Circus model)
        {
            if (String.IsNullOrWhiteSpace(model.Name))
                throw new FormatException("Attention ! Le nom d'un cirque ne peut être vide ou null");
            if (String.IsNullOrWhiteSpace(model.Address))
                throw new FormatException("Attention ! L'addresse d'un cirque ne peut être vide ou null");
            if (String.IsNullOrWhiteSpace(model.Country))
                throw new FormatException("Attention ! Le pays d'un cirque ne peut être vide ou null");
            if (String.IsNullOrWhiteSpace(model.City))
                throw new FormatException("Attention ! La ville d'un cirque ne peut être vide ou null");

            using (var context = this._dbFactory.GetContext())
            {
                Circus create = context.Circuses.Add(new Circus()
                {
                    Name = model.Name,
                    Address = model.Address,
                    Country = model.Country,
                    City = model.City,
                    Description = model.Description
                });
                context.SaveChanges();

                var result = create;

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(create, idx => idx
                    .Index("circusesmanage-circusindex")
                    .Type("Circus")
                    .Id(create.CircusId)
                    );
                return result;
            }

        }

        public IEnumerable<Circus> GetCircuses()
        {
            var usctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                IEnumerable<Circus> circuses = context.Circuses.Where(e => e.CircusId == usctx.CircusId).ToList();

                return circuses;
            }
        }

        public Circus GetCircusById(int id)
        {
            var usrctx = this._userCtx.Get();
            using (var context = this._dbFactory.GetContext())
            {
                Circus c = context.Circuses
                    .FirstOrDefault(e => e.CircusId == id && e.CircusId == usrctx.CircusId);
                if (c == null)
                    throw new Exception("Attention ! Le cirque d'un dresseur doit être en base");
                return c;
            }
        }
        public int UpdateCircus(Circus model)
        {
            if (String.IsNullOrWhiteSpace(model.Name))
                throw new FormatException("Attention ! Le nom d'un cirque ne peut être vide ou null");
            if (String.IsNullOrWhiteSpace(model.Address))
                throw new FormatException("Attention ! L'addresse d'un cirque ne peut être vide ou null");
            if (String.IsNullOrWhiteSpace(model.Country))
                throw new FormatException("Attention ! Le pays d'un cirque ne peut être vide ou null");
            if (String.IsNullOrWhiteSpace(model.City))
                throw new FormatException("Attention ! La ville d'un cirque ne peut être vide ou null");

            using (var context = this._dbFactory.GetContext())
            {
                Circus c = context.Circuses.FirstOrDefault(e => e.CircusId == model.CircusId);

                if (c == null)
                    throw new Exception("Erreur il n'ya pas de cirque specifiés");

                if (!c.Trainers.Any(e => e.TrainerId == _userCtx.Get().CircusId))
                    throw new Exception("Vous n'etes pas autoriser à modifier ce cirque");

                c.Name = model.Name;
                c.Address = model.Address;
                c.Country = model.Country;
                c.City = model.City;
                c.Description = model.Description;

                context.SaveChanges();

                var result = c.CircusId;

                var json = new
                {
                    c.Name,
                    c.Address,
                    c.Country,
                    c.City,
                    c.Description
                };

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Index(json, u => u
                    .Index("circusesmanage-circusindex")
                    .Type("Circus")
                    .Id(result));

                return result;
            }
        }

        public void DeleteCircus(int id)
        {
            var usrctx = this._userCtx.Get();

            if (!this._userCtx.Get().IsAdmin)
                throw new Exception("Vous n'êtes pas autorisé à suprimer des comptes.");

            using (var context = this._dbFactory.GetContext())
            {
                Circus c = context.Circuses.FirstOrDefault(e => e.CircusId == id && e.CircusId == usrctx.CircusId);

                if (c == null)
                    throw new Exception("Attention ! Le AnimalId d'un animal doit être en base");

                var exists = context.Animals.Any(e => e.AnimalId == e.FatherId);
                if (exists)
                    throw new Exception("Attention ! le AnimalID est associé au FatherID d'un animal");

                context.Circuses.Remove(c);

                context.SaveChanges();

                var node = new Uri("http://localhost:9200");
                var settings = new ConnectionSettings(node);
                var client = new ElasticClient(settings);

                var response = client.Delete<Circus>(id, d => d
                    .Index("circusesmanage-circusindex")
                    .Type("Circus"));

            }
        }
    }
}
