﻿using Sidi.CircusApp.Services.UserManagement;
using Sidi.CircusApp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sidi.CircusApp.UI.WebPortal.Controllers
{
    public class UsersController : Controller
    {
        public JsonResult GetAvailableUsers()
        {
            IEnumerable<Trainer> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IUserBusiness>();
                result = service.GetUsers();
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true,
                        data = result.Select(e => new
                        {
                            e.Name,
                            e.User,
                            e.Password
                        })
                    },
                    JsonRequestBehavior.AllowGet
                );
        }
        public JsonResult ConnectMe(string identifier, string passwd)
        {
            Trainer result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<IUserBusiness>();
                result = service.AuthMethod(identifier, passwd);
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true,
                    },
                    JsonRequestBehavior.AllowGet
                );
        }

        
    }
}