﻿using Sidi.CircusApp.Data.Entities;
using Sidi.CircusApp.Services.CircusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sidi.CircusApp.UI.WebPortal.Controllers
{
    public class CircusesController : Controller
    {
        // GET: Circuses
        public ActionResult Index()
        {
            IEnumerable<Circus> result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ICircusBusiness>();
                result = service.GetCircuses();
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Circus model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ICircusBusiness>();
                service.CreateCircus(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            Circus result = null;
            try
            {
                var service = DependencyResolver.Current.GetService<ICircusBusiness>();
                result = service.GetCircusById(id);
            }
            catch (Exception)
            {
                return View();
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult Update(Circus model)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ICircusBusiness>();
                service.UpdateCircus(model);
            }
            catch (Exception)
            {
                return View(model);
            }
            return RedirectToAction("Details");
        }

        public JsonResult Delete(int id)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<ICircusBusiness>();
                service.DeleteCircus(id);
            }
            catch (Exception)
            {
                return Json(
                        new
                        {
                            success = false
                        }
                    );
            }
            return Json(
                    new
                    {
                        success = true
                    }
                );
        }
    }
}